/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base.logger;

import net.telecomnancy.tp_schwartz_base.Config;


public class Logger {
    public static void log(LogLevel level, String message){
        switch(level){
            case ERROR : 
                error(message);
                break;
            case WARNING : 
                warning(message);
                break;
            case INFO : 
                info(message);
                break;
            case DEBUG : 
                debug(message);
                break;
            case TRACE : 
                trace(message);
                break;
        }
    }
    public static void error(String message, Exception e){
        if(Config.getLogLevel().getLevel()>=LogLevel.ERROR.getLevel()){
            System.out.println("ERROR :"+message);
            e.printStackTrace();
        }
    }
    public static void error(String message){
        if(Config.getLogLevel().getLevel()>=LogLevel.ERROR.getLevel()){
            System.out.println("ERROR :"+message);
        }
    }
    public static void warning(String message){
        if(Config.getLogLevel().getLevel()>=LogLevel.WARNING.getLevel()){
            System.out.println("WARNING :"+message);
        }
        
    }
    public static void info(String message){
        if(Config.getLogLevel().getLevel()>=LogLevel.INFO.getLevel()){
            System.out.println("INFO :"+message);
        }
        
    }
    public static void debug(String message){
        if(Config.getLogLevel().getLevel()>=LogLevel.DEBUG.getLevel()){
            System.out.println("DEBUG :"+message);
        }
        
    }
    public static void trace(String message){
        if(Config.getLogLevel().getLevel()>=LogLevel.TRACE.getLevel()){
            System.out.println("TRACE :"+message);
        }
        
    }
}
