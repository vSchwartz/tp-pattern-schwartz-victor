/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base.logger;


public enum LogLevel {
    ERROR(0),
    WARNING(1),
    INFO(2),
    DEBUG(3),
    TRACE(4);
    
    private final int level;
    private LogLevel(int level){
        this.level=level;
    }

    public int getLevel() {
        return level;
    }
}
