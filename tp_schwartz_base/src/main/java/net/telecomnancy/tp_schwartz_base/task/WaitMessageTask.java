/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base.task;

import java.util.concurrent.Callable;
import net.telecomnancy.tp_schwartz_base.rabbitmq.Receiver;



public class WaitMessageTask implements Callable<String>{
    private final Receiver receiver;
    public WaitMessageTask(Receiver receiver){
        this.receiver=receiver;
    }
    
    //@Override
    public String call() throws Exception {
        String rep=null;
        while(true){
            try{
                rep=receiver.get();
            }catch(NullPointerException e){}
            if(rep!=null){
                return rep;
            }
            Thread.sleep(10);
        }
    }
}
