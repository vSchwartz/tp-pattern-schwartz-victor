/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base.task;

import java.util.concurrent.Callable;
import net.telecomnancy.tp_schwartz_base.rabbitmq.Receiver;


public class ReceiverTask implements Callable<Object>{
    private final String host;
    private final int port;
    private final String queue;
    private final Receiver.ReceiverConsumer rc;
    public ReceiverTask(String host, int port,String queue, Receiver.ReceiverConsumer rc){
        super();
        this.host=host;
        this.port=port;
        this.queue=queue;
        this.rc=rc;
    }
    @Override
    public Object call() throws Exception {
        if(host==null || port==0 || queue==null || rc==null){
            throw new Exception("Missing parameter to create receiver");
        }
        Receiver receiver = new Receiver(host, port, queue, rc);
        return null;
    }
    
}
