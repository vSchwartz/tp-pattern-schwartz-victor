/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base.task;

import java.util.concurrent.Callable;
import net.telecomnancy.tp_schwartz_base.rabbitmq.Receiver;

/**
 *
 * @author sam
 */
public class WorkerTask implements Callable<Object>{
    public interface Worker{
        abstract void run(String msg);
    }
    private final Worker worker;
    private final String msg;
    public WorkerTask(Worker worker, String msg){
        this.worker=worker;
        this.msg=msg;
    }
    
    //@Override
    public Object call() throws Exception {
        worker.run(msg);
        return null;
    }
}