/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base;

public enum Param {
    DOUBLE("Double",Double.class,new Parser() {
        @Override
        public Object parse(String str) throws Exception{
            Double obj;
            try{
               obj = Double.parseDouble(str);
               return obj;
            }catch(Exception e){
                throw new Exception("Error on parsing");
            }
        }
    }),
    INTEGER("Integer",Integer.class,new Parser() {
        @Override
        public Object parse(String str) throws Exception{
            Integer obj;
            try{
               obj = Integer.parseInt(str);
               return obj;
            }catch(Exception e){
                throw new Exception("Error on parsing");
            }
        }
    }),
    BOOLEAN("Boolean",Boolean.class,new Parser() {
        @Override
        public Object parse(String str) throws Exception{
            Boolean obj;
            try{
                try{
                    Integer i= Integer.parseInt(str);
                    if(i==0){
                        return false;
                    } else{
                        return true;
                    }
                }catch(Exception e){
                }
               obj = Boolean.parseBoolean(str);
               return obj;
            }catch(Exception e){
                throw new Exception("Error on parsing");
            }
        }
    }),
    STRING("String",String.class,new Parser() {
        @Override
        public Object parse(String str)  throws Exception{
            return str;
        }
    });
        
        private final String name;
        private final Class<?> clazz;
        private final Parser parser;
        private interface Parser{
            abstract Object parse(String str)throws Exception;
        }
        private Param(String name, Class<?> clazz, Parser parser){
            this.name=name;
            this.clazz=clazz;
            this.parser=parser;
        }

    public String getName() {
        return name;
    }

    public Class<?> getClazz() {
        return clazz;
    }
    public Object parse(String str) throws Exception{
        return parser.parse(str);
    }
}
