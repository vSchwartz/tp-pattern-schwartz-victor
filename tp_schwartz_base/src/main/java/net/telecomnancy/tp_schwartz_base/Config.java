/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import net.telecomnancy.tp_schwartz_base.logger.LogLevel;
import org.omg.CORBA.TIMEOUT;


public class Config {
    private static final String KEY_WORKER_NUMBER = "WORKER_NUMBER";
    private static final String KEY_QUEUE_NAME = "QUEUE_NAME";
    private static final String KEY_EXCHANGE_NAME_PREFIX = "EXCHANGE_NAME_PREFIX";
    private static final String KEY_RABBIT_MQ_HOST = "RABBIT_MQ_HOST";
    private static final String KEY_RABBIT_MQ_PORT = "RABBIT_MQ_PORT";
    private static final String KEY_CLIENT_TIMEOUT_SEC = "CLIENT_TIMEOUT_SEC";
    private static final String KEY_LOG_LEVEL = "LOG_LEVEL";
    
    
    private static String queueName = "schwartz";
    private static String exchangeNamePrefix = "schwartz.";
    private static int workerNumber = 1;
    private static String rabbitMQHost = "ec2-54-157-2-56.compute-1.amazonaws.com";
    private static int rabbitMQPort = 5672;
    private static int clientTimeout = 5000;
    private static LogLevel logLevel = LogLevel.INFO;
    
    
    public static void init() throws IOException, Exception{
        Path path = Paths.get("./config.json");
        InputStream in = Files.newInputStream(path);
        StringBuilder  stringBuilder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line = null;
        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
        }
        JsonParser jp = new JsonParser();
        JsonObject jo = jp.parse(stringBuilder.toString()).getAsJsonObject();
        setConfig(jo);
    }

    public static String getExchangeNamePrefix() {
        return exchangeNamePrefix;
    }

    public static int getWorkerNumber() {
        return workerNumber;
    }

    public static String getQueueName() {
        return queueName;
    }

    public static LogLevel getLogLevel() {
        return logLevel;
    }

    public static String getRabbitMQHost() {
        return rabbitMQHost;
    }

    public static int getRabbitMQPort() {
        return rabbitMQPort;
    }

    public static int getClientTimeout() {
        return clientTimeout;
    }
    public static boolean haveClientTimeout(){
        return clientTimeout>0;
    }

    private static void setConfig(JsonObject jo) throws Exception {        
        if(jo.has(KEY_LOG_LEVEL)){
            String value = jo.get(KEY_LOG_LEVEL).getAsString();
            if(value.compareToIgnoreCase(LogLevel.ERROR.name())==0){
                logLevel=LogLevel.ERROR;
            }else if(value.compareToIgnoreCase(LogLevel.WARNING.name())==0){
                logLevel=LogLevel.WARNING;
            }else if(value.compareToIgnoreCase(LogLevel.INFO.name())==0){
                logLevel=LogLevel.INFO;
            }else if(value.compareToIgnoreCase(LogLevel.DEBUG.name())==0){
                logLevel=LogLevel.DEBUG;
            }else if(value.compareToIgnoreCase(LogLevel.TRACE.name())==0){
                logLevel=LogLevel.TRACE;
            }else {
                logLevel=LogLevel.INFO;
            }
        }else{
            throw new Exception("Missing configuration :"+KEY_LOG_LEVEL);
        }
        if(jo.has(KEY_RABBIT_MQ_HOST)){
            rabbitMQHost = jo.get(KEY_RABBIT_MQ_HOST).getAsString();
        }else{
            throw new Exception("Missing configuration :"+KEY_RABBIT_MQ_HOST);
        }
        if(jo.has(KEY_RABBIT_MQ_PORT)){
            rabbitMQPort = jo.get(KEY_RABBIT_MQ_PORT).getAsInt();
        }else{
            throw new Exception("Missing configuration :"+KEY_RABBIT_MQ_PORT);
        }
        if(jo.has(KEY_QUEUE_NAME)){
            queueName = jo.get(KEY_QUEUE_NAME).getAsString();
        }else{
            throw new Exception("Missing configuration :"+KEY_QUEUE_NAME);
        }
        if(jo.has(KEY_WORKER_NUMBER)){
            workerNumber = jo.get(KEY_WORKER_NUMBER).getAsInt();
        }else{
            throw new Exception("Missing configuration :"+KEY_WORKER_NUMBER);
        }
        if(jo.has(KEY_EXCHANGE_NAME_PREFIX)){
            exchangeNamePrefix = jo.get(KEY_EXCHANGE_NAME_PREFIX).getAsString();
        }else{
            throw new Exception("Missing configuration :"+KEY_EXCHANGE_NAME_PREFIX);
        }
        //TimeOut Optionnel
        if(jo.has(KEY_CLIENT_TIMEOUT_SEC)){
            clientTimeout = jo.get(KEY_CLIENT_TIMEOUT_SEC).getAsInt();
        }else{
            clientTimeout = -1;
        }
        
    }
    
}
