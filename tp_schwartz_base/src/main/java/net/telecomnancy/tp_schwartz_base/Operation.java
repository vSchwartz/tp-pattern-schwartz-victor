/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base;

import java.util.ArrayList;
import java.util.List;

public enum Operation {
    ADDITION("Addition", Param.DOUBLE, Param.DOUBLE),
    SUBSTRACTION("Substraction", Param.DOUBLE, Param.DOUBLE),
    MULTIPLICATION("Multiplication", Param.DOUBLE, Param.DOUBLE),
    DIVISION("Division", Param.DOUBLE, Param.DOUBLE);
    private final String name;
    private final List<Param> attrs = new ArrayList<Param>();
    
    Operation(String name, Param ... attrs){
        this.name=name;
        for(Param attr:attrs){
            this.attrs.add(attr);
        }
    }

    public String getName() {
        return name;
    }

    public List<Param> getAttrs() {
        return attrs;
    }
    
}
