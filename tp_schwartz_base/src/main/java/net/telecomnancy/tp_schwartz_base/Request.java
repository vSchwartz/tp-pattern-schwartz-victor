/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base;

import com.google.gson.Gson;
import java.rmi.Remote;
import java.util.ArrayList;
import java.util.List;
import net.telecomnancy.tp_schwartz_base.logger.Logger;


public class Request implements Remote{
    Operation ope;
    List<String> attrs = new ArrayList<String>();
    int waitingTime;
    String exchangeName;
    public Request() {
    }

    public Request(Operation ope) {
        this.ope = ope;
    }
    
    public Request(Operation ope, List<String> attrs) {
        this.ope = ope;
        this.attrs.addAll(attrs);
    }
    
    public Request(Operation ope, List<String> attrs, String exchangeName) {
        this.ope = ope;
        this.attrs.addAll(attrs);
        this.exchangeName=exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public List<String> getAttrs() {
        return attrs;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public Operation getOpe() {
        return ope;
    }

    public void setAttrs(List<String> attrs) {
        this.attrs.clear();
        this.attrs.addAll(attrs);
    }

    public void setOpe(Operation ope) {
        this.ope = ope;
    }
    public Object getAttr(int i) throws Exception{
        return ope.getAttrs().get(i).parse(attrs.get(i));
    }

    
    public String toJson(){
        Logger.trace("Converting to JSON :"+this.toString());
        Gson gson = new Gson();
        String json = gson.toJson(this);
        Logger.trace("JSON :" + json);
        return json;
    }
    public static Request jsonToRequest(String str){
        Logger.trace("Parsing from JSON :"+str);
        Gson gson = new Gson();
        Request req = gson.fromJson(str, Request.class);
        Logger.trace("Object :"+req.toString());
        return req;
    }
}
