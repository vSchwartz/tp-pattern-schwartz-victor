/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base.rabbitmq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeoutException;
import net.telecomnancy.tp_schwartz_base.Config;
import net.telecomnancy.tp_schwartz_base.logger.Logger;


public class Receiver {
    
    private Connection connection;
    private Channel channel;
    private String host;
    private int port;
    private String queue;
    private String exchangeName;
    private String consumerTag;
    
    /*public static void createTaskReceiver(){
        Task rec = new Task() {
            Receiver receiver;
            @Override
            protected Object call() throws Exception {
                receiver = new Receiver(host, 0, queue, consumer)
            }
        })
    }*/
    public interface ReceiverConsumer{
        abstract void run(String msg);
    }
    //temporaireReceiver (1 ecoute)
    public Receiver(String host, int port) throws  IOException, TimeoutException{
        Logger.debug("Create receiver for "+host+":"+port);
        this.host=host;
        this.port=port;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        connection = factory.newConnection();
        channel = connection.createChannel();
        
        this.exchangeName=generateExchangeName();
        channel.exchangeDeclare(this.exchangeName, "fanout");
        this.queue = channel.queueDeclare().getQueue();
        channel.queueBind(this.queue, this.exchangeName, "");
        Logger.debug("Create receiver : OK");
    }
    public Receiver(String host, int port,String queue, ReceiverConsumer rc) throws  IOException, TimeoutException{
        Logger.debug("Create receiver for "+host+":"+port+" On queue :"+queue);
        this.host=host;
        this.port=port;
        this.queue=queue;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        connection = factory.newConnection();
        channel = connection.createChannel();
        channel.queueDeclare(Config.getQueueName(), false, false, false, null);
        this.consumerTag=generateConsumerTag();
        channel.basicConsume(queue, true, this.consumerTag,getConsumer(channel, rc));
        Logger.debug("Create receiver : OK");
    }
    private Consumer getConsumer(Channel channel,ReceiverConsumer rc){
       return new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                    String message = new String(body, "UTF-8");
                    rc.run(message);
                }
            };
    }
    public void deleteQueue() throws IOException{
        this.channel.queueDelete(this.queue);
    }
    public String getHost() {
        return host;
    }

    public String get() throws Exception{
        return new String(channel.basicGet(queue, true).getBody(), "UTF-8");
    }
    public String getExchangeName() {
        return exchangeName;
    }

    public int getPort() {
        return port;
    }

    public String getQueue() {
        return queue;
    }
    public void close() throws IOException, TimeoutException{
            if(this.exchangeName!=null){
                channel.queueUnbind(this.queue, this.exchangeName, "");
            }
            if(this.consumerTag!=null){
                channel.basicCancel(this.consumerTag);
            }
            channel.close();
            connection.close();
    }
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if(this.exchangeName!=null){
            channel.queueUnbind(this.queue, this.exchangeName, "");
        }
        if(this.consumerTag!=null){
            channel.basicCancel(this.consumerTag);
        }
        channel.close();
        connection.close();
    }
    
    private static String generateExchangeName(){
        Integer i = (new Random(System.currentTimeMillis()).nextInt());
        return Config.getExchangeNamePrefix()+i;
    }
    
    private static String generateConsumerTag(){
        Integer i = (new Random(System.currentTimeMillis()).nextInt());
        return "consumer."+i;
    }
}
