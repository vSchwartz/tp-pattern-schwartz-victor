/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_base.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import net.telecomnancy.tp_schwartz_base.Request;
import net.telecomnancy.tp_schwartz_base.logger.Logger;

public class Producer {
    private Connection connection;
    private Channel channel;
    
    public Producer(String host, int port) throws  IOException, TimeoutException{
        Logger.debug("Create producer for "+host+":"+port);
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setPort(port);
        connection = factory.newConnection();
        channel = connection.createChannel();
        Logger.debug("Create producer : OK");
    }
    public void close() throws IOException, TimeoutException{
        channel.close();
        connection.close();
    }
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        channel.close();
        connection.close();
    }
    public void publishRequest(String queue, Request req) throws  IOException{
        publishMsg(queue, req.toJson(), false);
    }
    public void publishMsg(String name, String msg, boolean exchangeName) throws  IOException{
        Logger.debug("Publish message on :"+name);
        if(exchangeName){
            channel.exchangeDeclare(name, "fanout");
            channel.basicPublish(name, "", null, msg.getBytes());
        }else{
            channel.queueDeclare(name, false, false, false, null);
            channel.basicPublish("", name, null, msg.getBytes());
        }
        Logger.debug("Publish message : OK");
        Logger.trace("message : "+msg);
        
    }
}
