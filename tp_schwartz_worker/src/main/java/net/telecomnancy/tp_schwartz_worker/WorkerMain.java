/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_worker;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import net.telecomnancy.tp_schwartz_base.Config;
import net.telecomnancy.tp_schwartz_base.Operation;
import net.telecomnancy.tp_schwartz_base.Request;
import net.telecomnancy.tp_schwartz_base.logger.Logger;
import net.telecomnancy.tp_schwartz_base.rabbitmq.Producer;
import net.telecomnancy.tp_schwartz_base.rabbitmq.Receiver;
import net.telecomnancy.tp_schwartz_base.task.ReceiverTask;
import net.telecomnancy.tp_schwartz_base.task.WorkerTask;
import net.telecomnancy.tp_schwartz_serviceweb.Calculette_Service;
import net.telecomnancy.tp_schwartz_serviceweb.Calculette;

public class WorkerMain {
    
    public static void main (String [] args) throws Exception{
        try {
            Config.init();
        } catch (Exception e) {
            Logger.error("Initialisation failed ",e);
            return;
        }
        int i;
        System.out.println("start receiver");
        try{
            Receiver receiver = new Receiver(Config.getRabbitMQHost(), Config.getRabbitMQPort(), Config.getQueueName(), msg->createWorker(msg));
        }catch(Exception e){
            Logger.error("Fail to start receiver ",e);
            return;
        }
    }
    
    public static Calculette startWebservice() throws Exception{
        try{
            Calculette_Service service = new Calculette_Service();
            Calculette port = service.getCalculettePort();
            return port;
        }catch(Exception e){
            throw new Exception("Fail to initiate webService.",e);
        }
    }
    public static Double callAddition(Request request,Calculette port) throws Exception{
        Double x,y;
        Operation ope = Operation.ADDITION;
        verifyRequest(request, ope);
        x=(Double)request.getAttr(0);
        y=(Double)request.getAttr(1);
       return port.addition(x, y);
    }
    public static Double callSubstraction(Request request,Calculette port) throws Exception{
        Double x,y;
        Operation ope = Operation.SUBSTRACTION;
        verifyRequest(request, ope);
        x=(Double)request.getAttr(0);
        y=(Double)request.getAttr(1);
        return port.substraction(x, y);
    }
    public static Double callMultiplication(Request request,Calculette port) throws Exception{
        Double x,y;
        Operation ope = Operation.MULTIPLICATION;
        verifyRequest(request, ope);
        x=(Double)request.getAttr(0);
        y=(Double)request.getAttr(1);
        return port.multiplication(x, y);
    }
    public static Double callDivision(Request request,Calculette port) throws Exception{
        Double x,y;
        Operation ope = Operation.DIVISION;
        verifyRequest(request, ope);
        x=(Double)request.getAttr(0);
        y=(Double)request.getAttr(1);
        return port.division(x, y);
    }
    public static void verifyRequest(Request request, Operation ope) throws Exception{
        if(request.getOpe()!=ope){
            throw new Exception("Wrong called function");
        }
        if(request.getAttrs().size()!=ope.getAttrs().size()){
            throw new Exception("Incorrect number of parameter");
        }
    }
    
    public static void createWorker(String msg){
        Logger.debug("create worker for:"+msg);
        ExecutorService pool = Executors.newSingleThreadExecutor();
        WorkerTask rt = new WorkerTask(str->execRequest(str), msg);
        pool.submit(rt);
    }
    public static void execRequest(String msg){
        Calculette port;
        try {
            port = startWebservice();
        } catch (Exception e) {
            Logger.error("Fail start web service", e);
            return;
        }
        Request request = Request.jsonToRequest(msg);
        try {
            Thread.sleep(request.getWaitingTime()*1000);
        } catch (InterruptedException e) {
            Logger.error("Error to wait requested time", e);
            sendException(request.getExchangeName(),"Error to wait requested time", e);
            return;
        }
        Object result=null;
        try{
        switch(request.getOpe()){
            case ADDITION : result=callAddition(request,port);break;
            case SUBSTRACTION : result=callSubstraction(request,port);break;
            case MULTIPLICATION : result=callMultiplication(request,port);break;
            case DIVISION : result=callDivision(request,port);break;
        }
        }catch(Exception e){
            Logger.error("Error to call webService", e);
            sendException(request.getExchangeName(),"Error to call webService", e);
        }
        if(result!=null){
            try{
                sendResult(request.getExchangeName(),result);
                Thread.sleep(5000);
            }catch(Exception e){
                Logger.error("Fail to send answer", e);
                sendException(request.getExchangeName(),"Fail to send answer", e);
            }
        }
    }
    public static void sendResult(String name,Object result) throws IOException, TimeoutException{
        Producer producer = new Producer(Config.getRabbitMQHost(), Config.getRabbitMQPort());
        producer.publishMsg(name, result.toString(),true);
        producer.close();
    }
    
    public static void sendException(String name,String message, Exception ex){
        try{
            Producer producer = new Producer(Config.getRabbitMQHost(), Config.getRabbitMQPort());
            producer.publishMsg(name, message+" : "+ex.getMessage(), true);
            producer.close();
        }catch(Exception e){}
    }
}
