package net.telecomnancy.tp_schwartz_serviceweb;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;


@WebService(serviceName = "Calculette")
@Stateless()
public class Calculette {

    
    @WebMethod(operationName = "addition")
    public Double addition(@WebParam(name = "x") Double x, @WebParam(name = "y") Double y) {
        return x+y;
    }

    @WebMethod(operationName = "substraction")
    public Double substraction(@WebParam(name = "x") Double x, @WebParam(name = "y") Double y) {
        return x-y;
    }

    
    @WebMethod(operationName = "multiplication")
    public Double multiplication(@WebParam(name = "x") Double x, @WebParam(name = "y") Double y) {
        return x*y;
    }

    
    @WebMethod(operationName = "division")
    public Double division(@WebParam(name = "x") Double x, @WebParam(name = "y") Double y) {
        return x/y;
    }

    
}
