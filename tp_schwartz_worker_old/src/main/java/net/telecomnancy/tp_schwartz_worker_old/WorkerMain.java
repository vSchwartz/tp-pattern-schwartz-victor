/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_worker_old;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import net.telecomnancy.tp_schwartz_base.Config;
import net.telecomnancy.tp_schwartz_base.Operation;
import net.telecomnancy.tp_schwartz_base.Request;
import net.telecomnancy.tp_schwartz_base.logger.Logger;
import net.telecomnancy.tp_schwartz_base.rabbitmq.Producer;
import net.telecomnancy.tp_schwartz_base.task.ReceiverTask;

public class WorkerMain {
    private static net.telecomnancy.tp_schwartz_serviceweb.Calculette port ;
    
    public static void main (String [] args) throws Exception{
        try {
            Config.init();
            startWebservice();
        } catch (Exception e) {
            Logger.error("Initialisation failed ",e);
            return;
        }
        int i;
        System.out.println("start worker");
        List<Future<Object>> workers = new ArrayList<Future<Object>>(Config.getWorkerNumber());
        try{
            
            for(i=1; i<=Config.getWorkerNumber();i++){
                ExecutorService pool = Executors.newSingleThreadExecutor();
                ReceiverTask rt = new ReceiverTask(Config.getRabbitMQHost(), Config.getRabbitMQPort(), Config.getQueueName(), msg->execRequest(msg));
                Future<Object> future = pool.submit(rt);
                future.get();
                workers.add(future);
            }
            System.out.println((i-1)+" worker started");
        }catch(Exception e){
            Logger.error("Fail to start worker ",e);
            for( Future<Object> worker : workers){
                worker.cancel(true);
            }
            return;
        }
    }
    
    public static void startWebservice() throws Exception{
        try{
            net.telecomnancy.tp_schwartz_serviceweb.Calculette_Service service = new net.telecomnancy.tp_schwartz_serviceweb.Calculette_Service();
            port = service.getCalculettePort();
        }catch(Exception e){
            throw new Exception("Fail to initiate webService.",e);
        }
    }
    public static Double callAddition(Request request) throws Exception{
        Double x,y;
        Operation ope = Operation.ADDITION;
        verifyRequest(request, ope);
        x=(Double)request.getAttr(0);
        y=(Double)request.getAttr(1);
       return port.addition(x, y);
    }
    public static Double callSubstraction(Request request) throws Exception{
        Double x,y;
        Operation ope = Operation.SUBSTRACTION;
        verifyRequest(request, ope);
        x=(Double)request.getAttr(0);
        y=(Double)request.getAttr(1);
        return port.substraction(x, y);
    }
    public static Double callMultiplication(Request request) throws Exception{
        Double x,y;
        Operation ope = Operation.MULTIPLICATION;
        verifyRequest(request, ope);
        x=(Double)request.getAttr(0);
        y=(Double)request.getAttr(1);
        return port.multiplication(x, y);
    }
    public static Double callDivision(Request request) throws Exception{
        Double x,y;
        Operation ope = Operation.DIVISION;
        verifyRequest(request, ope);
        x=(Double)request.getAttr(0);
        y=(Double)request.getAttr(1);
        return port.division(x, y);
    }
    public static void verifyRequest(Request request, Operation ope) throws Exception{
        if(request.getOpe()!=ope){
            throw new Exception("Wrong called function");
        }
        if(request.getAttrs().size()!=ope.getAttrs().size()){
            throw new Exception("Incorrect number of parameter");
        }
    }
    public static void execRequest(String msg){
        Request request = Request.jsonToRequest(msg);
        try {
            Thread.sleep(request.getWaitingTime()*1000);
        } catch (InterruptedException e) {
            Logger.error("Error to wait requested time", e);
            sendException(request.getExchangeName(),"Error to wait requested time", e);
            return;
        }
        Object result=null;
        try{
        switch(request.getOpe()){
            case ADDITION : result=callAddition(request);break;
            case SUBSTRACTION : result=callSubstraction(request);break;
            case MULTIPLICATION : result=callMultiplication(request);break;
            case DIVISION : result=callDivision(request);break;
        }
        }catch(Exception e){
            Logger.error("Error to call webService", e);
            sendException(request.getExchangeName(),"Error to call webService", e);
        }
        if(result!=null){
            try{
                sendResult(request.getExchangeName(),result);
                Thread.sleep(5000);
            }catch(Exception e){
                Logger.error("Fail to send answer", e);
                sendException(request.getExchangeName(),"Fail to send answer", e);
            }
        }
    }
    public static void sendResult(String name,Object result) throws IOException, TimeoutException{
        Producer producer = new Producer(Config.getRabbitMQHost(), Config.getRabbitMQPort());
        producer.publishMsg(name, result.toString(),true);
        producer.close();
    }
    
    public static void sendException(String name,String message, Exception ex){
        try{
            Producer producer = new Producer(Config.getRabbitMQHost(), Config.getRabbitMQPort());
            producer.publishMsg(name, message+" : "+ex.getMessage(), true);
            producer.close();
        }catch(Exception e){}
    }
}
