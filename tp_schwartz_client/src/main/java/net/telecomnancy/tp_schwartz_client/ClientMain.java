/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.telecomnancy.tp_schwartz_client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import net.telecomnancy.tp_schwartz_base.Config;
import net.telecomnancy.tp_schwartz_base.Operation;
import net.telecomnancy.tp_schwartz_base.Param;
import net.telecomnancy.tp_schwartz_base.Request;
import net.telecomnancy.tp_schwartz_base.logger.Logger;
import net.telecomnancy.tp_schwartz_base.rabbitmq.Producer;
import net.telecomnancy.tp_schwartz_base.rabbitmq.Receiver;
import net.telecomnancy.tp_schwartz_base.task.WaitMessageTask;


public class ClientMain {
    private static final Integer EXIT=0;
    private static Producer producer;
    private static final Map<Integer, Operation> choice = new HashMap<>();
    private static  Scanner sc;
    public static void main(String [] args){
        System.out.println("Welcome to the customer of TP_SDIS. This client has been designed as part of a project in Distributed System. (Developped by Victor Schwartz)");
        try {
            init();
        } catch (Exception e) {
            Logger.error("Initialisation failed ",e);
            return;
        }
        sc = new Scanner(System.in);
        Integer choix;
        while(true){
            afficherMenu();
            choix=null;
            try{
                choix = Integer.parseInt(sc.nextLine());
                if(choix.compareTo(EXIT)==0){
                    break;
                }
            }catch(Exception e){
                Logger.error("Incorrect choice.",e);
            }
            if(choix!=null){
                try{
                    List<String> params = afficherOperation(choice.get(choix));
                    int waitingTime = Integer.parseInt(params.remove(0));
                    Request request = new Request(choice.get(choix), params);
                    request.setWaitingTime(waitingTime);
                    sendRequest(request);
                }catch(Exception e){
                    Logger.error("Error on call function :"+e.getMessage(),e);
                }
            }
        }
        sc.close();
        try {
            producer.close();
        } catch (IOException | TimeoutException e) {
            Logger.error("Error on closing producer :",e);
        }
    }
    private static void init() throws Exception{
        int i = 1;
        Config.init();
        for(Operation ope : Operation.values()){
            choice.put(i, ope);
            i++;
        }
        producer = new Producer(Config.getRabbitMQHost(), Config.getRabbitMQPort());
    }
    private static void afficherMenu(){
        System.out.println("Choose a function :");
        System.out.print("Exit ("+EXIT+")");
        int i = 1;
        for(Map.Entry<Integer,Operation> ope : choice.entrySet()){
            System.out.print(", "+ope.getValue().getName()+"("+ope.getKey()+")");
        }
        System.out.println(".");
    }
    private static List<String> afficherOperation(Operation ope) throws Exception{
        //demande du temps d'attente n (exigence sujet);
        sc = new Scanner(System.in);
        List<String> params = new ArrayList<>(ope.getAttrs().size());
        Boolean timeOk=false;
        do{
            try{
                System.out.println("Waiting time (seconde):");
                Integer i = Integer.parseInt(sc.nextLine());
                if(i>=0){
                    params.add(i.toString());
                    timeOk=true;
                }else{
                    throw new Exception("Error negative value.");
                }
            }catch(Exception e){
                Logger.error(e.getMessage());
            }
        }while(!timeOk);
        System.out.println("Function parameter :");
        System.out.print(ope.getName()+"(");
        for(Param param : ope.getAttrs()){
             System.out.print(param.getName()+" ");
        }
        System.out.println(")");
        for(Param param : ope.getAttrs()){
            System.out.println(param.getName()+":");
            params.add(param.parse(sc.nextLine()).toString());
            Logger.trace("Value : "+params.get(params.size()-1));
        }
        return params;
    }
    private static void sendRequest(Request request) throws Exception{
        long startTime ;
        Receiver receiver=null;
        try{
            receiver = new Receiver(Config.getRabbitMQHost(), Config.getRabbitMQPort());
            request.setExchangeName(receiver.getExchangeName());
            startTime = System.currentTimeMillis();
            producer.publishRequest(Config.getQueueName(), request);
        }catch(Exception e){
            throw new Exception("Error to send request",e);
        }
        try {
            System.out.println("Waiting ...");
            String answer = getAnswer(receiver,request.getWaitingTime());
            long total = System.currentTimeMillis()-startTime;
            System.err.println("Reponse : "+answer +" (execute on :"+(total)+"ms)");
            System.out.println("Request time : n->"+(request.getWaitingTime()*1000)+"ms, m->"+(total-(request.getWaitingTime()*1000))+"ms" );
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
           Logger.error("Error on waiting answer :",e);
        }
        try {
            receiver.close();
        }catch(Exception e){
            Logger.error("Error on closing receiver :",e);
        }
    }
    private static String getAnswer(Receiver receiver, int waitingTime) throws InterruptedException, ExecutionException, TimeoutException{
        
        ExecutorService pool = Executors.newSingleThreadExecutor();
        WaitMessageTask wmt = new WaitMessageTask(receiver);
        Future<String> future = pool.submit(wmt);
        if(Config.haveClientTimeout()){
            return future.get(Config.getClientTimeout()+waitingTime, TimeUnit.SECONDS);
        }else{
            return  future.get();
        }
    }
}
